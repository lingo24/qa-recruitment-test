# Lingo24 QA Recruitment Test

## Requirements
* Java SDK v1.8
* Maven v3
* Firefox (Windows and Linux webdrivers are included in this project)

## Maven commands

To build and run the project use:

	mvn clean test
	
## Features

The main feature files are included in `src/test/resources/features`.
