package com.lingo24.qarecruitmenttest.core;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Spring configuration.
 */
@Configuration
@ComponentScan(basePackages = { "com.lingo24.qarecruitmenttest" })
public class BddConfiguration
{

}
