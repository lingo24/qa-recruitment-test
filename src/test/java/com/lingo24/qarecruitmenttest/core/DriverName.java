package com.lingo24.qarecruitmenttest.core;

import lombok.Getter;

@Getter
public enum DriverName
{

    WINDOWS_FIREFOX("geckodriver_win32.exe"),
    WINDOWS_CHROME("chromedriver_win32.exe"),
    WINDOWS_PHANTOMJS("phantomjs.exe"),
    UNIX_FIREFOX("geckodriver"),
    UNIX_CHROME("chromedriver"),
    MAC_CHROME("chromedriver_mac32.exe"),
    UNIX_PHANTOMJS("phantomjs");

    private String name;

    DriverName(String name)
    {
        this.name = name;
    }
}
