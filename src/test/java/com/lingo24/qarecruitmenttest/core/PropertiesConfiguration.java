package com.lingo24.qarecruitmenttest.core;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
public class PropertiesConfiguration implements EnvironmentAware
{
    private Environment environment;

    /**
     * Load the application properties files specific to the active spring profiles, falling back to the defaults if
     * there are no active profiles.
     *
     * @return A configured property source placeholder configurer for the active / default profiles.
     * @throws IOException If there is an error loading the properties.
     */
    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() throws IOException
    {
        List<Resource> resources = new ArrayList<>();
        Collections.addAll(resources,
            new PathMatchingResourcePatternResolver().getResources("classpath*:/*-common.properties"));

        String currentUser = System.getProperty("user.name");
        currentUser = currentUser.replaceAll("([^a-zA-Z0-9])", "_").toLowerCase();

        if (environment.getActiveProfiles().length == 0) {
            for (String profile : environment.getDefaultProfiles()) {
                Collections.addAll(resources,
                    new PathMatchingResourcePatternResolver().getResources("classpath*:/*-" + profile + ".properties"));
                Collections.addAll(resources, new PathMatchingResourcePatternResolver()
                    .getResources("classpath*:/*-" + profile + "-" + currentUser + ".properties"));
            }
        } else {
            for (String profile : environment.getActiveProfiles()) {
                Collections.addAll(resources,
                    new PathMatchingResourcePatternResolver().getResources("classpath*:/*-" + profile + ".properties")

                );
                Collections.addAll(resources, new PathMatchingResourcePatternResolver()
                    .getResources("classpath*:/*-" + profile + "-" + currentUser + ".properties"));
            }
        }

        PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
        pspc.setLocations(resources.toArray(new Resource[0]));
        return pspc;
    }

    @Override
    public void setEnvironment(Environment environment)
    {
        this.environment = environment;
    }
}
