package com.lingo24.qarecruitmenttest.core;

import static org.openqa.selenium.Platform.WIN8;
import static org.openqa.selenium.Platform.WIN8_1;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Setup and teardown functions.
 */
@Slf4j
public class Setup
{
    @Autowired
    WebDriverService webDriverService;


    @After
    public void teardown(Scenario scenario)
    {
        try {

            if (scenario.isFailed()) {
                final byte[] screenshot = ((TakesScreenshot) webDriverService.getWebDriver())
                    .getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            }

            Platform platform = Platform.getCurrent();

            if( webDriverService.getWebDriver() instanceof FirefoxDriver
                && (platform == WIN8 || platform == WIN8_1)) {
                //TODO make it kill a specific process instead of any firefox process
                Runtime.getRuntime().exec("taskkill /F /IM firefox.exe");
                Thread.sleep(2000);
                Runtime.getRuntime().exec("taskkill /f /im plugin-container.exe");
                Runtime.getRuntime().exec("taskkill /f /im WefFault.exe");

            } else {
                webDriverService.getWebDriver().quit();
            }

            webDriverService.setWebDriver(null);
        } catch (Exception e) {
            log.error("Could not close web driver.", e);
        }
    }
}
