package com.lingo24.qarecruitmenttest.core;

import com.google.common.base.Strings;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;

@Slf4j
@Service
public class WebDriverService
{
    @Setter
    private static WebDriver webDriver;

    // Waits in seconds
    public static final int SHORT_WAIT  = 10;
    public static final int MEDIUM_WAIT = 20;
    public static final int LONG_WAIT   = 30;

    public void initWebDriver() throws Exception
    {
        String browser = System.getProperty("browser");
        String hubUrl  = System.getProperty("hubUrl");

        if (Strings.isNullOrEmpty(hubUrl)) {
            getLocalBrowser(browser);
        } else {
            getRemoteBrowser(hubUrl, browser);
        }

        //webDriver.manage().window().maximize();
        webDriver.manage().window().setSize(new Dimension(1920, 1080));
        log.info("Browser Height {} and Width {}", webDriver.manage().window().getSize().getHeight(), webDriver
            .manage().window().getSize().getWidth());
    }

    public WebDriver getWebDriver() throws Exception
    {
        if (null == webDriver) {
            initWebDriver();
        }
        return webDriver;
    }

    private void getRemoteBrowser(String hubUrl, String browser) throws Exception
    {
        DesiredCapabilities capabilities;

        if (null == browser || browser.toLowerCase().equals(BrowserType.FIREFOX)) {
            capabilities = DesiredCapabilities.firefox();
        } else if (browser.toLowerCase().equals(BrowserType.CHROME)) {
            capabilities = DesiredCapabilities.chrome();
        } else if (browser.toLowerCase().equals(BrowserType.IE) || browser.toLowerCase().equals("ie")) {
            capabilities = DesiredCapabilities.internetExplorer();
        } else if (browser.toLowerCase().equals("edge")) {
            capabilities = DesiredCapabilities.edge();
        } else {
            capabilities = DesiredCapabilities.firefox();
            FirefoxProfile fprofile = new FirefoxProfile();

            fprofile.setAcceptUntrustedCertificates(true);
            fprofile.setAssumeUntrustedCertificateIssuer(true);

            fprofile.setPreference("browser.download.folderList", 2);
            fprofile.setPreference("browser.helperApps.alwaysAsk.force", false);
            fprofile.setPreference("browser.download.manager.showWhenStarting", false);
            fprofile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
            capabilities.setCapability(FirefoxDriver.PROFILE, fprofile);


        }

        String version = System.getProperty("version");
        if (null != version) {
            capabilities.setVersion(version);
        }

        String platform = System.getProperty("platform");
        if (null != platform) {
            capabilities.setPlatform(Platform.fromString(platform));
        }

        capabilities.setJavascriptEnabled(true);

        RemoteWebDriver remoteWebDriver = new RemoteWebDriver(new URL(hubUrl), capabilities);
        remoteWebDriver.setFileDetector(new LocalFileDetector());
        webDriver = remoteWebDriver;

    }

    private void getLocalBrowser(String browser)
    {
        Platform platform = Platform.getCurrent();

        if (null == browser || browser.toLowerCase().equals(BrowserType.FIREFOX)) {
            webDriver = getFirefoxDriver(platform);
        } else if (browser.toLowerCase().equals(BrowserType.PHANTOMJS)) {
            webDriver = getPhantomJSDriver(platform);
        } else if (browser.toLowerCase().equals(BrowserType.CHROME)) {
            webDriver = getChromeDriver(platform);
        } else if (browser.toLowerCase().equals(BrowserType.IE) || browser.toLowerCase().equals("ie")) {
            String driverUri = WebDriverService.class.getClassLoader().getResource("webdrivers/IEDriverServer_Win32.exe")
                .getPath();
            System.setProperty("webdriver.ie.driver", driverUri);
            webDriver = new InternetExplorerDriver();

        } else if (browser.toLowerCase().equals("edge")) {
            webDriver = new EdgeDriver();
        } else {
            log.warn("Could not find browser type for \"{}\".", browser);
            webDriver = getFirefoxDriver(platform);
        }
    }

    private PhantomJSDriver getPhantomJSDriver(Platform platform)
    {

        String path;
        try {
            String driverName;

            switch (platform) {

                case WINDOWS:
                case WIN8:
                case WIN8_1:
                case WIN10:
                    driverName = DriverName.WINDOWS_PHANTOMJS.getName();
                    break;
                case LINUX:
                case MAC:
                    driverName = DriverName.UNIX_PHANTOMJS.getName();
                    break;
                default:
                    driverName = DriverName.UNIX_PHANTOMJS.getName();
            }

            path = getDriverPath(driverName);
        } catch (IOException e) {
            log.error("Unable to get the driver path or driver name", e);
            throw new IllegalStateException("Unable to initialise web driver", e);
        }

        System.setProperty("phantomjs.binary.path", path);

        DesiredCapabilities capabilities = DesiredCapabilities.phantomjs();
        capabilities.setCapability("webdriver.log.driver", "DEBUG");
        capabilities.setCapability("unhandledPromptBehavior", "accept");
        PhantomJSDriver driver = new PhantomJSDriver(capabilities);
        driver.manage().window().setSize(new Dimension(1920, 1080));
        return driver;

    }

    private String getDriverPath(String driverName) throws IOException
    {
        ClassLoader classLoader = WebDriverService.class.getClassLoader();
        URL resource = classLoader.getResource("webdrivers/" + driverName);

        return resource.getPath();
    }

    private FirefoxDriver getFirefoxDriver(Platform platform)
    {

        String path;
        try {
            String driverName;

            switch (platform) {

                case WINDOWS:
                case WIN10:
                case WIN8:
                case WIN8_1:
                    driverName = DriverName.WINDOWS_FIREFOX.getName();
                    break;
                case LINUX:
                case MAC:
                    driverName = DriverName.UNIX_FIREFOX.getName();
                    break;
                default:
                    driverName = DriverName.UNIX_FIREFOX.getName();
            }
            path = getDriverPath(driverName);
        } catch (IOException e) {
            log.error("Unable to get the driver path or driver name", e);
            throw new IllegalStateException("Unable to initialise web driver", e);
        }

        System.setProperty("webdriver.gecko.driver", path);

        FirefoxProfile fprofile = new FirefoxProfile();

        fprofile.setAcceptUntrustedCertificates(true);
        fprofile.setAssumeUntrustedCertificateIssuer(true);

        fprofile.setPreference("browser.download.folderList", 2);
        fprofile.setPreference("browser.helperApps.alwaysAsk.force", false);

        fprofile.setPreference("browser.download.dir", "D:\\Webdriverdownloads");
        fprofile.setPreference("browser.download.defaultFolder", "D:\\Webdriverdownloads");
        fprofile.setPreference("browser.download.manager.showWhenStarting", false);

        fprofile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");

        return new FirefoxDriver(fprofile);
    }

    private WebDriver getChromeDriver(Platform platform)
    {

        String path;
        try {
            String driverName;
            if (platform.is(Platform.WINDOWS)|| platform.is(Platform.WIN8)||platform.is(Platform.WIN8_1)||platform.is
                (Platform.WIN10)) {
                driverName = DriverName.WINDOWS_CHROME.getName();
            } else if (platform.is(Platform.UNIX)) {
                driverName = DriverName.UNIX_CHROME.getName();
            } else if (platform.is(Platform.MAC)) {
                driverName = DriverName.MAC_CHROME.getName();
            } else {
                log.warn("Could not determine chrome driver for {} platform. Returning win32 driver", platform);
                driverName = DriverName.WINDOWS_CHROME.getName();
            }

            path = getDriverPath(driverName);
        } catch (IOException e) {
            log.error("Unable to get the driver path or driver name", e);
            throw new IllegalStateException("Unable to initialise web driver", e);
        }

        System.setProperty("webdriver.chrome.driver", path);
        return new ChromeDriver();
    }
}
