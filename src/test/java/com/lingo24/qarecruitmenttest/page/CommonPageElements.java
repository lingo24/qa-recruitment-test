package com.lingo24.qarecruitmenttest.page;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.lingo24.qarecruitmenttest.core.WebDriverService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URL;

@Slf4j
public class CommonPageElements
{
    @Getter
    protected WebDriver driver;

    public CommonPageElements(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void gotToUrl(String urlToGoTo)
    {
        driver.get(urlToGoTo);
    }

    public void refreshPage() {
        getDriver().navigate().refresh();
    }

    public String getPageTitle()
    {
        return getDriver().getTitle();
    }

    public void waitUntilElementIsVisible(By by, int timeoutSec)
    {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeoutSec);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public boolean isElementVisible(By by)
    {
        return getDriver().findElement(by).isDisplayed();
    }

    public WebElement findByCssSelector(String selector)
    {
        return getDriver().findElement(By.cssSelector(selector));
    }

    public void waitForElementToBeVisible(String element)
    {
        waitForElementToBeVisible(By.id(element));
    }

    public void waitForElementToBeVisible(By findBy)
    {
        WebDriverWait wait = new WebDriverWait(getDriver(), WebDriverService.SHORT_WAIT);
        wait.until(ExpectedConditions.visibilityOfElementLocated(findBy));

    }

    public void waitForPageToLoad(String pageName)
    {
        WebDriverWait wait = new WebDriverWait(getDriver(),20);
        try{
            wait.until(ExpectedConditions.titleContains(pageName));
            assertTrue(getDriver().getTitle().contains(pageName));
        }catch (Exception e){
            fail(e.getMessage());
        }
    }


    public void waitForElementToBeClickable(WebElement webElement)
    {
        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        wait.until(ExpectedConditions.and(ExpectedConditions.visibilityOf(webElement),
            ExpectedConditions.elementToBeClickable(webElement)));
    }

    public void waitForElementToBeClickable(By findBy)
    {
        waitForElementToBeClickable(findBy, 10);
    }

    public void waitForElementToBeClickable(By findBy, long timeout)
    {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeout);
        wait.until(ExpectedConditions.and(ExpectedConditions.visibilityOfElementLocated(findBy),
            ExpectedConditions.elementToBeClickable(findBy)));
    }


    public void waitForJavascriptToLoad()
    {
        ExpectedCondition<Boolean> pageLoadCondition = driver -> ((JavascriptExecutor) driver)
            .executeScript("return document.readyState").equals("complete");
        WebDriverWait wait = new WebDriverWait(getDriver(), WebDriverService.SHORT_WAIT);
        wait.until(pageLoadCondition);
    }

    public void setValueOfSelect2AdvancedFilter(String value, String selectSelector)
    {
        By findBy = By.xpath("//*[@id=\"" + selectSelector + "\"]//span[@role=\"combobox\"]");
        waitForElementToBeClickable(findBy);
        WebElement select2Filter = getDriver().findElement(findBy);
        select2Filter.click();
        getDriver().switchTo().activeElement().sendKeys(value);
        getDriver().switchTo().activeElement().sendKeys(Keys.ENTER);
    }

    public void setValueOfSelect2(String value, String selectSelector)
    {
        By findBy = By.cssSelector(selectSelector + " + span");
        waitForElementToBeClickable(findBy);

        WebElement element =  getDriver().findElement(findBy);
        element.click();

        try {
            getDriver().switchTo().activeElement().sendKeys(value);
            getDriver().switchTo().activeElement().sendKeys(Keys.ENTER);
        } catch (Exception e) {
            setValueOfSelectById(value, selectSelector);
        }
    }

    public void setValueOfSelectById(String value, String id)
    {
        By findBy = By.id("s2id_" + id);
        waitForElementToBeClickable(findBy);
        WebElement selector = getDriver().findElement(findBy);
        selector.click();

        try {
            getDriver().switchTo().activeElement().sendKeys(value);
            getDriver().switchTo().activeElement().sendKeys(Keys.ENTER);
        } catch (Exception e) {
            WebElement input = getDriver().findElement(By.id(id));
            input.sendKeys(value);
            input.sendKeys(Keys.ENTER);
        }

    }

    public void enterTextIntoTheField(String value, String fieldName)
    {
        By findBy = By.xpath(
            "//input[contains(@name,\"" + fieldName + "\")] | //div[./label[text()=\"" + fieldName
                + "\"]]/following::div/input | //*[contains(text(),\"" + fieldName + "\")]/../following-sibling::*//textarea | //*[contains(text(),\"" + fieldName + "\")]/../following-sibling::*/*/input" );
        waitForElementToBeClickable(findBy, 20);
        WebElement field = getDriver().findElement(findBy);
        field.clear();
        if (fieldName.startsWith("Email")) {
            field.sendKeys(RandomStringUtils.randomAlphabetic(8) + "_" + value);
        } else {
            field.sendKeys(value);
        }
    }

    public void enterTextIntoTheFieldWithId(String value, String fieldId)
    {
        By findBy = By.id(fieldId);
        waitForElementToBeClickable(findBy, 30);
        WebElement field = getDriver().findElement(findBy);
        field.sendKeys(value);
    }

    public void clickMenuItem(String item)
    {
        By findBy = By
            .xpath("//nav//span[contains(text()," + "\"" + item + "\")] | " + "//a[contains(.,\"" + item + "\")]");
        waitForElementToBeClickable(findBy, 20);
        WebElement menu = getDriver().findElement(findBy);

        if (menu != null) {
            menu.click();
        }
    }

    public void clickTabItem(String item)
    {
        By findBy = By.xpath
            ("//ul[@class=\"tabs-nav\"]//span[contains(text()," + "\"" + item + "\")]");//
        // TODO have taken this extra bit out as it seemed to select the wrong element when there
        // are multiple elements named Projects
        // | //a[contains(.,"+ "\"" // + item + "\")]");
        waitForElementToBeClickable(findBy, 20);
        WebElement tab = getDriver().findElement(findBy);

        if (tab != null) {
            tab.click();
        }
    }

    public WebElement getLink(String linkHref)
    {
        By findBy = By.xpath("//a[@href=\"" + linkHref + "\"] | //a[@href=\"/" + linkHref + "\"]");
        waitForElementToBeClickable(findBy,20);
        return getDriver().findElement(findBy);
    }

    public void clickTheLink(String linkHref)
    {
        WebElement link;

        try {
            link = getLink(linkHref);
        } catch (NoSuchElementException e) {
            link = null;
        }

        if (link != null) {
            link.click();
        }
    }

    public void clickTheButton(String buttonLabel)
    {
        By findBy = By.xpath(
            "//button[@id=\" " + buttonLabel + "\"] | //button[contains(text(),\"" + buttonLabel
                + "\")] |  //a[@class='button' and contains(text(),\"" + buttonLabel + "\")]");
        waitForElementToBeClickable(findBy);
        WebElement link = getDriver().findElement(findBy);

        if (link != null) {
            link.click();
        }
    }

    public void uploadFile(String cssSelector, File file) throws Exception
    {
        String filePath = file.getAbsolutePath();

        if (getDriver() instanceof PhantomJSDriver) {
            ((PhantomJSDriver) getDriver())
                .executePhantomJS("var page = this; page.uploadFile('" + cssSelector + " ', '" + filePath + "');");
        } else {
            WebElement uploadFilesSection = getDriver().findElement(By.cssSelector(cssSelector));
            uploadFilesSection.sendKeys(filePath);
        }
    }

    public void uploadFile(String elementName, String fileName) throws Exception
    {
        URL fileUrl = ClassLoader.getSystemResource(fileName);
        uploadFile(elementName, new File(fileUrl.toURI()));

        shortWait();
    }

    public void closeCookiePopupDialog()
    {
        try {
            WebElement hidePopupsButton = getDriver().findElement(By.cssSelector(".cc-compliance .cc-dismiss"));

            if (hidePopupsButton != null) {
                JavascriptExecutor js = (JavascriptExecutor) getDriver();
                js.executeScript("arguments[0].click();", hidePopupsButton);
            }
        } catch (NoSuchElementException e) {
            log.trace("No pop-ups to close, skip", e);
        }
    }

    public void shortWait() throws InterruptedException
    {
        Thread.sleep(WebDriverService.SHORT_WAIT * 1000);
    }

    public void mediumWait() throws InterruptedException
    {
        Thread.sleep(WebDriverService.MEDIUM_WAIT * 1000);
    }

    public void longWait() throws InterruptedException
    {
        Thread.sleep(WebDriverService.LONG_WAIT * 1000);
    }
    public void clickSubMenuItem(String item)
    {
        By findBy = By.xpath(
            "//*[contains(@class,\"menuItem\") and contains(.,\"" + item
                + "\")]");
        waitForElementToBeClickable(findBy, 20);
        WebElement menu = getDriver().findElement(findBy);

        if (menu != null) {
            menu.click();
        }
    }

    public void waitForMessageToBeDisplayed(String message)
    {
        By findBy = By.xpath("//div[text()=\"" + message + "\"] "
            + "| //h2[text()=\"" + message + "\"] "
            + "| //h1[text()=\"" + message + "\"] "
            + "| //div[@class=\"error\" and text()=\"" + message + "\"]"
            + "| //b[contains(.,\"" + message + "\")]");
        waitUntilElementIsVisible(findBy,20);
        WebElement div = getDriver().findElement(findBy);

        assertNotNull(div);
        assertTrue(div.isDisplayed());
    }

}