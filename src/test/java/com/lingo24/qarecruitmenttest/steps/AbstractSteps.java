package com.lingo24.qarecruitmenttest.steps;

import static org.junit.Assert.fail;

import com.lingo24.qarecruitmenttest.core.BddConfiguration;
import com.lingo24.qarecruitmenttest.core.WebDriverService;
import com.lingo24.qarecruitmenttest.page.CommonPageElements;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

/**
 * Common step definition requirements.
 */
@ContextConfiguration(classes = {BddConfiguration.class})
public abstract class AbstractSteps
{
    @Value("${web.base.url}")
    @Getter
    private String webBaseUrl;

    @Autowired
    @Getter
    private WebDriverService webDriverService;

    public WebDriver getWebDriver()
    {
        try {
            return getWebDriverService().getWebDriver();
        } catch (Exception e) {
            fail("Could not load WebDriver: " + e.getMessage());
        }

        return null;
    }

    public void shortWait() throws InterruptedException
    {
        new CommonPageElements(getWebDriver()).shortWait();
    }

    public void mediumWait() throws InterruptedException
    {
        new CommonPageElements(getWebDriver()).mediumWait();
    }

    public void longWait() throws InterruptedException
    {
        new CommonPageElements(getWebDriver()).longWait();
    }
}
