package com.lingo24.qarecruitmenttest.steps;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.lingo24.qarecruitmenttest.page.CommonPageElements;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URISyntaxException;

/**
 * Common form interaction steps.
 */
@Slf4j
public class Forms extends AbstractSteps
{
    @When("^I click the (.*) button$")
    public void i_click_the___button(String buttonLabel) throws Throwable
    {
        By findBy = By.xpath("//button[@id=\" " + buttonLabel + "\"] | //button[contains(text(),\"" + buttonLabel + "\")] |  //a[@class='button' and contains(text(),\"" + buttonLabel + "\")]");
        new CommonPageElements(getWebDriver()).waitUntilElementIsVisible(findBy,20);
        new CommonPageElements(getWebDriver()).waitForElementToBeClickable(findBy,20);
        WebElement button =  getWebDriver().findElement(findBy);

        if (button != null) {
            button.click();
        }
    }

    @When("^I click (.*) button from (.*) table$")
    public void i_click___button_from_table(String buttonLabel, String tableName) throws Throwable
    {
        By findBy = By.xpath("//*[contains(@class,'title')][contains(text(),\""+ tableName+ "\")]/../following::button[contains(text(),\"" + buttonLabel +"\")] | //*[contains(@class,'title')][contains(text(),\""+ tableName+ "\")]/../following::button[contains(@name,\"" + buttonLabel + "\")]");
        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        new CommonPageElements(getWebDriver()).waitForElementToBeClickable(findBy);
        WebElement button = getWebDriver().findElement(findBy);

        if (button != null) {
            button.click();
        }
    }

    @When("^I click the (.*) button from (.*) sub-tab")
    public void i_click_the___button_from___sub_tab(String buttonTable, String tabName)
    {
        By findBy = By.xpath("//div[@style='display:inline;']/div/div[@style='display:inline;' and contains(@id,\"" + tabName + "\")]//button[contains(text(),\"" + buttonTable + "\")]");
        new CommonPageElements(getWebDriver()).waitForElementToBeClickable(findBy);
        WebElement button = getWebDriver().findElement(findBy);

        button.click();
    }

    @And("^I click the (.*) field$")
    public void i_click_the___field(String fieldName) {
        By findBy = By.cssSelector("input[name=\"" + fieldName + "\"]");
        new CommonPageElements(getWebDriver()).waitForElementToBeClickable(findBy);
        WebElement field = getWebDriver().findElement(findBy);

        field.click();
    }


    @Then("^The (.*) field is displayed$")
    public void the___field_is_displayed(String fieldName) throws Throwable
    {
        By findBy = By.cssSelector("input[name=\"" + fieldName + "\"]");
        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        WebElement field = getWebDriver().findElement(findBy);

        assertNotNull(field);
        assertTrue(field.isDisplayed());
    }

    @Then("^The (.*) button is displayed$")
    public void the___button_is_displayed(String buttonLabel) throws Throwable
    {
        By findBy = By.xpath("//button[@id=\"" + buttonLabel + "\"] | //button[text" + "()=\"" + buttonLabel + "\"] | //button[contains(text(),\"" + buttonLabel + "\")]");
        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        WebElement field = getWebDriver().findElement(findBy);

        assertNotNull(field);
        assertTrue(field.isDisplayed());
    }

    @Then("^The (.*) label is displayed$")
    public void the___label_is_displayed(String label)
    {
        By findBy = By.xpath("//td[@class=\"title\"  and contains(text(),\"" + label + "\")]");
        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        WebElement field = getWebDriver().findElement(findBy);

        assertNotNull(field);
        assertTrue(field.isDisplayed());
    }

    @When("^I enter (.*) into the (.*) field$")
    public void i_enter___into_the___field(String value, String fieldName) throws Throwable
    {
        new CommonPageElements(getWebDriver()).enterTextIntoTheField(value, fieldName);
    }

    @When("^I enter (.*) into the field with id (.*)$")
    public void i_enter___into_the_field_with_id___(String value, String fieldId) throws Throwable
    {
        new CommonPageElements(getWebDriver()).enterTextIntoTheFieldWithId(value, fieldId);
    }

    @When("^I select (.*) from the (.*) selector")
    public void i_select___from_the___selector(String value, String id)
    {
        new CommonPageElements(getWebDriver()).setValueOfSelectById(value, id);
    }

    @When("^I enter (.*) into the (.*) selector")
    public void i_enter___into_the___selector(String value, String id)
    {
        new CommonPageElements(getWebDriver()).setValueOfSelect2(value, "#" + id);
    }

    @When("^I select (.*) from the (.*) dropdown$")
    public void i_select___from_the___dropdown(String text, String id)
    {
        By findBy = By.xpath( "//select[@id=\"" + id + "\"] | //div[./label[text()=\""+ id +
            "\"]]/following-sibling::div/select | //span//label[contains(.,\"" + id + "\")]/../../../following-sibling::span/select | //*[contains(text(),\"" +id + "\")]/../following-sibling::*/span/select");
        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        new CommonPageElements(getWebDriver()).waitForElementToBeClickable(findBy);
        Select select = new Select(getWebDriver().findElement(findBy));
        select.selectByVisibleText(text);
    }

    @When("^I add file (.*) to (.*)$")
    public void i_add_file___to___(String file, String input) throws URISyntaxException {
        getWebDriver().findElement(By.xpath("//*[@id=\"" + input + "\"] | //div[@class='left']/label[contains(., 'Upload file')]/../following-sibling::div[@class='right']/div/span/input")).sendKeys(
            new File(Forms.class.getClassLoader().getResource("files/" + file).getFile()).getAbsolutePath());
    }

    @When("^I enter (.*) into the (.*) select field$")
    public void i_enter___into_the____select_field(String value, String selectSelector)
    {
        By findBy = By.cssSelector(selectSelector + " + span");
        new CommonPageElements(getWebDriver()).waitForElementToBeClickable(findBy);
        WebElement selectElement = getWebDriver().findElement(findBy);
        selectElement.click();
        getWebDriver().switchTo().activeElement().sendKeys(value);
        getWebDriver().switchTo().activeElement().sendKeys(Keys.ENTER);
    }

    @And("^I click the OK button from popup alert$")
    public void  i_click_the_OK_button_from_popup_alert()  {
        WebDriverWait wait = new WebDriverWait(getWebDriver(),20);

        wait.until(ExpectedConditions.alertIsPresent());
        try {
            getWebDriver().switchTo().alert().accept();
        }catch (Exception e){
        }
    }

    @Then("^The (.*) image is displayed$")
    public void the___image_is_displayed(String image) {
        By findBy = By.xpath("//img[@alt=\"" + image + "\"]");
        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        WebElement field = getWebDriver().findElement(findBy);

        assertNotNull(field);
        assertTrue(field.isDisplayed());
    }

    @And("^I click on (.*) checkbox from (.*) table")
    public void i_click_on___checkbox_from___table(String checkbox, String tableName)
    {
        By findBy =  By.xpath("//*[@class='titleBar' or @class='title'][contains(text(),\"" + tableName + "\")]/../following::label[text()=\"" + checkbox + "\"]/../../td/input[@type='checkbox']");

        new CommonPageElements(getWebDriver()).waitForElementToBeClickable(findBy);
        WebElement checkboxElement =  getWebDriver().findElement(findBy);

        if (checkboxElement != null) {
            checkboxElement.click();
        }
    }

    @Then("^Value for (.*) field is not (.*)$")
    public void value_for___field_is_not___(String fieldName, String value)
    {
        By findBy = By.xpath("//*[contains(text(),\"" + fieldName + "\")]/../following-sibling::*/*" );

        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        WebElement field  = getWebDriver().findElement(findBy);

        assertFalse(field.getText().equals(value));
    }

    @Then("^I click (.*) item from list$")
    public void i_click___item_from_list(String item)
    {
        By findBy = By.xpath("//*[@role='listbox']//a[contains(.,\"" + item + "\")]");
        new CommonPageElements(getWebDriver()).waitForElementToBeClickable(findBy);
        WebElement element = getWebDriver().findElement(findBy);

        element.click();
    }
}