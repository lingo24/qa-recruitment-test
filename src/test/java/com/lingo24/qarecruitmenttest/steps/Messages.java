package com.lingo24.qarecruitmenttest.steps;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.lingo24.qarecruitmenttest.page.CommonPageElements;
import cucumber.api.java.en.Then;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Common message confirmation steps.
 */
@Slf4j
public class Messages extends AbstractSteps
{
    @Then("^A message with text (.*) is displayed$")
    public void a_message_with_text___is_displayed(String message) throws Throwable
    {

        By findBy = By.xpath("//div[text()=\"" + message + "\"] "
                + "| //h2[text()=\"" + message + "\"] "
                + "| //h1[text()=\"" + message + "\"] "
                + "| //div[@class=\"error\" and text()=\"" + message + "\"]"
                + "| //b[contains(.,\"" + message + "\")] "
                + "| //h3/span[text()=\"" + message + "\"] ");

        //*[@id="tasks-ready-for-invoice-message-container"]/div[1]/h3/span[2]

        new CommonPageElements(getWebDriver()).waitUntilElementIsVisible(findBy,20);
        WebElement div = getWebDriver().findElement(findBy);

        assertNotNull(div);
        assertTrue(div.isDisplayed());
    }

    @Then("^A success message is displayed$")
    public void a_success_message_is_displayed() throws Throwable
    {
        By findBy = By.xpath("//div[@id=\"successMsg\"]");
        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        WebElement div = getWebDriver().findElement(findBy);

        assertNotNull(div);
    }

    @Then("^A error message is displayed$")
    public void a_error_message_is_displayed() throws Throwable
    {
        By findBy = By.xpath("//div[@id=\"errorMsg\"]");
        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        WebElement div = getWebDriver().findElement(findBy);

        assertNotNull(div);
    }
}