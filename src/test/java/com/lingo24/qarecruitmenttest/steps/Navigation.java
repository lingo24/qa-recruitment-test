package com.lingo24.qarecruitmenttest.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.lingo24.qarecruitmenttest.core.WebDriverService;
import com.lingo24.qarecruitmenttest.page.CommonPageElements;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Common navigation steps.
 */
@Slf4j
public class Navigation extends AbstractSteps
{
    @Given("^I am an unauthenticated user on the homepage$")
    public void i_am_an_unauthenticated_user_on_the_homepage() throws Throwable
    {
        getWebDriver().get(getWebBaseUrl());

        try {
            WebElement hidePopupsButton = getWebDriver()
                .findElement(By.xpath("//div[@id=\"popup-container\"]//button[@data-action=\"hide-popups\"]"));

            if (hidePopupsButton != null) {
                JavascriptExecutor js = (JavascriptExecutor) getWebDriver();
                js.executeScript("arguments[0].click();", hidePopupsButton);
            }
        } catch (NoSuchElementException e) {
            log.trace("No pop-ups to close, skip", e);
        }
    }

    @When("^I zoom out (\\d+) times$")
    public void i_zoom_out___times(int times) throws Throwable
    {
        if(!GraphicsEnvironment.isHeadless()) {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            for(int i = 0; i< times; i++) {
                robot.keyPress(KeyEvent.VK_MINUS);
                robot.keyRelease(KeyEvent.VK_MINUS);
            }
            robot.keyRelease(KeyEvent.VK_CONTROL);
        }
    }

    @When("^I change the URL (.*) parameter to (.*)$")
    public void i_change_the_url___parameter_to__(String key, String value) throws Throwable
    {
        String url = getWebDriver().getCurrentUrl().replaceAll(key + "=[^&]+", key + "=" + value);
        getWebDriver().get(url);
    }

    @When("^I click the (.*) link$")
    public void i_click_the___link(String linkHref) throws Throwable
    {
        WebElement link;

        try {
            link = new CommonPageElements(getWebDriver()).getLink(linkHref);
        } catch (NoSuchElementException e) {
            link = getWebDriver().findElement(By.cssSelector("[href=\"" + getWebBaseUrl() + "/" + linkHref + "\"]"));
        }

        if (link != null) {
            link.click();
        }
    }
    @When("^I click (.*) field from (.*) table")
    public void i_click___field_from___table(String fieldName, String tableName)
    {
       WebElement field = getWebDriver().findElement(By.xpath("//*[@class='titleBar' or @class='title'][contains(text(),\"" + tableName + "\")]/../following::label[contains(.,\"" + fieldName + "\")]"));

        if (field != null) {
            field.click();
        }
    }

    @And("^I click on (.*) image from (.*) table$")
    public void i_click_on___image(String image, String table)
    {
        By findBy = By.xpath("//*[@class='titleBar' or @class='title'][contains(text(),\"" + table + "\")]/following-sibling::*/img[@class=\"" + image + "\"]");
        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        WebElement field = getWebDriver().findElement(findBy);

        field.click();
    }

    @When("^I click the (.*) tab")
    public void i_click_the___tab(String tabName)
    {
        new CommonPageElements(getWebDriver()).clickTabItem(tabName);
    }

    @When("^I select the (.*) menu")
    public void i_select_the___menu(String item) throws Throwable
    {
        new CommonPageElements(getWebDriver()).clickMenuItem(item);
    }

    @Then("^I am on the (.*) page$")
    public void i_am_on_the___page(String pageName) throws Throwable
    {
        new CommonPageElements(getWebDriver()).waitForPageToLoad(pageName);
    }

    @Then("^The (.*) link is displayed$")
    public void the___link_is_displayed(String linkHref) throws Throwable
    {
        WebElement link = new CommonPageElements(getWebDriver()).getLink(linkHref);

        assertNotNull(link);
        assertTrue(link.isDisplayed());
    }

    @Then("^the (.*) tab is displayed$")
    public void the___tab_is_displayed(String title) throws Throwable
    {
        By findBy = By.xpath("//a[@role=\"tab\" and text()=\"" + title + "\"]");
        new CommonPageElements(getWebDriver()).waitForElementToBeVisible(findBy);
        WebElement tab = getWebDriver().findElement(findBy);

        assertNotNull(tab);
        assertTrue(tab.isDisplayed());
    }

    @When("^I wait for (.*) to be displayed$")
    public void i_wait_for___(String element)
    {
        WebDriverWait wait = new WebDriverWait(getWebDriver(), WebDriverService.SHORT_WAIT);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element)));
    }

    @When("^I wait for a short time$")
    public void i_wait_for_a_short_time() throws InterruptedException
    {
        shortWait();
    }
    @And("^The (.*) window title is displayed$")
    public void the___is_displayed(String window){
        WebDriverWait wait = new WebDriverWait(getWebDriver(),30);
        try{
            wait.until(ExpectedConditions.titleIs(window));
            assertEquals(getWebDriver().getTitle(), window);
        }catch (Exception e){
        }
    }

    @Then("^I click the (.*) sub-menu$")
    public void i_click_the___sub_menu(String submenu) throws Throwable {
        new CommonPageElements(getWebDriver()).clickSubMenuItem(submenu);
    }

    @Then("^I switch to (.*?) page$")
    public void i_switch_to___page(String pageName) throws Throwable {
        try{
            for(String winHandle : getWebDriver().getWindowHandles()){
                if(winHandle.contains(pageName)){
                    getWebDriver().switchTo().window(winHandle);
                }
            }
        }catch (Exception e){}
    }
}