package com.lingo24.qarecruitmenttest.steps;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by neil on 21/06/17.
 */
public class Tables extends AbstractSteps
{
    @Then("^the (.*) table is displayed$")
    public void the___table_is_displayed(String fieldName) throws Throwable
    {
        WebElement table = getWebDriver().findElement(By.id(fieldName));

        assertNotNull(table);
        assertTrue(table.isDisplayed());
    }
}
