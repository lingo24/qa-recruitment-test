@authentication
Feature: User authentication
  Standard login and logout functionality for the Ease portal.

Scenario Outline: Successful login
  Given I am an unauthenticated user on the homepage
  When  I click the login link
  Then  The username field is displayed
  And   The password field is displayed
  And   The Login button is displayed
  When  I enter <username> into the username field
  And   I enter <password> into the password field
  And   I click the Login button
  Then  I am on the Order | Ease | Lingo24 page

Examples:
  | username            | password |
  | example@example.com | XXXXXXXX |

Scenario Outline: Unsuccessful login
  Given I am an unauthenticated user on the homepage
  When  I click the login link
  Then  The username field is displayed
  And   The password field is displayed
  And   The Login button is displayed
  When  I enter <username> into the username field
  And   I enter <password> into the password field
  And   I click the Login button
  Then  I am on the Login page
  And   A message with text Invalid username and password. is displayed

Examples:
  | username            | password |
  | example@example.com | wrong    |
